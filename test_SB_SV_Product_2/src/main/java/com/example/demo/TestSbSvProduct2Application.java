package com.example.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@MapperScan("com.example.demo.mapper")
@SpringBootApplication
@ServletComponentScan
public class TestSbSvProduct2Application {

	public static void main(String[] args) {
		SpringApplication.run(TestSbSvProduct2Application.class, args);
	}

}
