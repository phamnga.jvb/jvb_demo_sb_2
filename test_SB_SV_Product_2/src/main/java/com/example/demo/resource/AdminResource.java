package com.example.demo.resource;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.mapper.AdminMapper;
import com.example.demo.model.Admin;
import com.example.demo.model.Catalog;
import com.example.demo.model.Product;
import com.example.demo.model.User;
import com.example.demo.service.AdminService;
import com.example.demo.service.CatalogService;
import com.example.demo.service.ProductService;

@Controller
@RequestMapping("/admin")
public class AdminResource {

	@Autowired
	private AdminService adminService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private CatalogService catalogService;
	
	/**
	 * @author Pham Nga
	 * Welcome Controller
	 * @return
	 * @since 03/04/2019
	 */
	@RequestMapping(value = {"/", "/welcome"})
	public String welcome (HttpSession session, Model model) {
		
		/*														*/
		/*				CHECK USERNAME ATTR TO SESSION			*/
		/*														*/
		String username = (String) session.getAttribute("username");
		
		if (username == null || !username.equals("admin"))
			return "redirect:/welcome";
		
		/*								 							*/
		/*			SET LIST-NEW-PRODUCT ATTR TO SESSION		   	*/
		/*															*/
		if (session.getAttribute("listNewProduct") == null) {
			List<Product> listNewProduct = productService.getListNewProduct();
			session.setAttribute("listNewProduct", listNewProduct);
		}
		
		/*												*/
		/*	GET CATALOG CHILD WITH PARENT CATALOG NAME	*/
		/*												*/
		List<Catalog> listMakeupCatalog = new ArrayList<>();
		List<Catalog> listSkinCareCatalog = new ArrayList<>();
		listMakeupCatalog = catalogService.getAllCatalogByNameParent("TRANG ĐIỂM");
		listSkinCareCatalog = catalogService.getAllCatalogByNameParent("CHĂM SÓC DA");
		
		/*															*/
		/*			GET LIST PRODUCT CHILD WITH EACH CATALOG 		*/
		/*															*/
		List<List<Product>> listMakeupProducts = new ArrayList<>();
		List<List<Product>> listSkinCareProducts = new ArrayList<>();
		
		for (Catalog catalog:listMakeupCatalog) {
			List<Product> list = new ArrayList<>();
		
			/*															*/
			/*			GET LIST CATALOG CHILD WITH EACH CATALOG 		*/
			/*															*/
			String makeUpID = catalog.getiD();
			List<Catalog> listCatalogChilds = new ArrayList<>();
			listCatalogChilds.add(catalog);
			listCatalogChilds.addAll(catalogService.getAllCatalogByOriginId(makeUpID));
			
			for (Catalog c:listCatalogChilds) {
				String iDCatalog = c.getiD();
				list.addAll(productService.getListProductByIdCatalog(iDCatalog));
			}
			
			listMakeupProducts.add(list);
		}
		
		for (Catalog catalog:listSkinCareCatalog) {
			List<Product> list = new ArrayList<>();
			
			/*															*/
			/*			GET LIST CATALOG CHILD WITH EACH CATALOG 		*/
			/*															*/
			String skinCareId = catalog.getiD();
			List<Catalog> listCatalogChilds = new ArrayList<>();
			listCatalogChilds.add(catalog);
			listCatalogChilds.addAll(catalogService.getAllCatalogByOriginId(skinCareId));
						
			for (Catalog c:listCatalogChilds) {
				String iDCatalog = c.getiD();
				list.addAll(productService.getListProductByIdCatalog(iDCatalog));
			}
			
			listSkinCareProducts.add(list);
		}
		
		/*								*/
		/*	GET ALL ATTRIBUTES TO MODEL */
		/*								*/
		model.addAttribute("listMakeupCatalog", listMakeupCatalog);
		model.addAttribute("listSkinCareCatalog", listSkinCareCatalog);
		model.addAttribute("listMakeupProducts", listMakeupProducts);
		model.addAttribute("listSkinCareProducts", listSkinCareProducts);
		
		return "index";
	}
	
	/**
	 * @author Pham Nga
	 * Login Controller
	 * @param session
	 * @return
	 * @since 03/04/2019
	 */
	@GetMapping("/login")
	public String login (HttpSession session) {
		session.setAttribute("username", "guest");
		
		return "Admin/DangNhap/dang-nhap";
	}
	
	/**
	 * @author PN
	 * Check Login Controller
	 * @param userName
	 * @param password
	 * @param req
	 * @param res
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/checkLogin", method = RequestMethod.POST)
	public String checkLogin (@RequestParam ("userName") String userName, @RequestParam ("password") String password,
			HttpServletRequest req, HttpServletResponse res, ModelMap model,
			HttpSession session) {
		res.setContentType("text/html");
		String result = "";

		try {
			PrintWriter writer = res.getWriter();
			res.setContentType("text/html; charset=UTF-8");
			req.setCharacterEncoding("UTF-8");
			res.setCharacterEncoding("UTF-8");

			Admin admin = new Admin();
			admin.setUserName(userName);
			admin.setPassword(password);

			if (adminService.isAdmin(admin)) {
				
				// REMOVE COMPLAIN WHEN LOGINED
				if (model.containsAttribute("complain"))
					model.remove("complain");
				
				result = "redirect:/admin/welcome";
				session.setAttribute("username", "admin");
			} else {
				
				// ADD COMPLAIN WHEN LOGIN FAILED
				model.addAttribute("complain", "Mật khẩu / Tên đăng nhập không chính xác!");
				result = "Admin/DangNhap/dang-nhap";
				session.setAttribute("username", "guest");
				//writer.println("Please check out Information!");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	/**
	 * @author PN
	 * View Admin Controller
	 * @return
	 */
	@RequestMapping(value = {"/view-admin"})
	public String getViewAdmin (HttpSession session) {
		
		/*														*/
		/*				CHECK USERNAME ATTR TO SESSION			*/
		/*														*/
		String username = (String) session.getAttribute("username");
		
		if (username == null || !username.equals("admin"))
			return "redirect:/welcome";
		
		return "Admin/QuanLy/tk-admin";
	}
	
	/**
	 * @author PN
	 * Log Out Controller
	 * @param session
	 * @return
	 */
	@RequestMapping(value = {"/logout"})
	public String logout (HttpSession session) {
		session.setAttribute("username", "guest");
		
		return "redirect:/welcome";
	}
	
	/**
	 * @author PN
	 * Return List Admin From DB
	 * @return listAdmin
	 * @since 03/04/2019
	 */
	/*
	@RequestMapping(value = {"/listAdmin"}, method = RequestMethod.POST)
	@ResponseBody
	public List<Admin> getAllAdmin () {
		List<Admin> listAdmin = adminService.getAllAdmin();
		
		System.out.println(listAdmin);
		
		return listAdmin;		
	}
	*/
	
	/**
	 * @author PN
	 * Return List Admin From DB
	 * @return listAdmin
	 * @since 03/04/2019
	 */
	@RequestMapping(value = {"/listAdmin"}, method = RequestMethod.GET)
	@ResponseBody
	public List<Admin> getAllAdmin (HttpSession session) {
		/*														*/
		/*				CHECK USERNAME ATTR TO SESSION			*/
		/*														*/
		String username = (String) session.getAttribute("username");
		
		if (username == null || !username.equals("admin"))
			return new ArrayList<>();
		
		List<Admin> listAdmin = adminService.getAllAdmin();
		
		System.out.println(listAdmin);
		
		return listAdmin;		
	}
	
	
	/******************************************/
	// 				For Testing 			 //
	/******************************************/

	@RequestMapping("/index")
	public String index () {
		
		return "index.jsp";
	}
	
	@RequestMapping(value = "/checkLogin2", method = RequestMethod.GET)
//	@ResponseBody
	public String checkLogin () {
		
		return "index";
		
	}
}
