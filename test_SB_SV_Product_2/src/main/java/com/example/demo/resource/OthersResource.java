package com.example.demo.resource;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.model.Catalog;
import com.example.demo.model.Product;
import com.example.demo.service.CatalogService;
import com.example.demo.service.ProductService;

@Controller
public class OthersResource {
	
	@Autowired 
	private ProductService productService;

	@Autowired
	private CatalogService catalogService;
	
	@RequestMapping(value = {"/tim-kiem"}, method = RequestMethod.GET, params = {"keyword"})
	public String finds (HttpServletRequest req, HttpServletResponse resp, 
			@RequestParam("keyword") String keyword, Model model) {
		List<Product> listFind = productService.getListProductByAString(keyword);
		List<Catalog> listCatalog = catalogService.getAllCatalog();
		List<Product> listNewProduct = productService.getListNewProduct();
		
		model.addAttribute("listFind", listFind);
		model.addAttribute("keyword", keyword);
		model.addAttribute("listCatalog", listCatalog);	
		model.addAttribute("listNewProduct", listNewProduct);
		
		return "tim-kiem";
	}
	
	@RequestMapping(value = {"/5-advances"})
	public String advances () {
		
		return "tin-tuc/5-loi-ich-tuyet-voi-khi-mua-sam-tai-he-thong-my-pham-so-huu-chuoi-cua-hang";
	}
	
	@RequestMapping(value = {"/review/review-dau-tay-trang-muji-mild-cleansing-oil-nhat-ban"})
	public String review_1 () {
		
		return "Review/review-dau-tay-trang-muji-mild-cleansing-oil-nhat-ban";
	}
	
	@RequestMapping(value = {"/review/review-cac-dong-serum-cua-timeless"})
	public String review_2 () {
		
		return "Review/review-cac-dong-serum-cua-timeless";
	}
	
	@RequestMapping(value = {"/review/review-phan-nuoc-april-skin-cuc-hot"})
	public String review_3 () {
		
		return "Review/review-phan-nuoc-april-skin-cuc-hot";
	}
	
	@RequestMapping(value = {"/review/review-nuoc-hoa-hong-evoluderm-lotion-tonique-chat-luong-cho-moi-loai-da"})
	public String review_4 () {
		
		return "Review/review-nuoc-hoa-hong-evoluderm-lotion-tonique-chat-luong-cho-moi-loai-da";
	}
	
	@RequestMapping(value = {"/review/review-danh-gia-mat-na-giay-innisfree-its-real-squeeze-mask"})
	public String review_5 () {
		
		return "Review/review-danh-gia-mat-na-giay-innisfree-its-real-squeeze-mask";
	}
	
	@RequestMapping(value = {"/review/review-nuoc-hoa-hong-khong-con-thayers-alcohol-free-witch-hazel-toner"})
	public String review_6 () {
		
		return "Review/review-nuoc-hoa-hong-khong-con-thayers-alcohol-free-witch-hazel-toner";
	}
	
	@RequestMapping(value = {"/review/review-kem-nen-than-thanh-maybelline-fit-me"})
	public String review_7 () {
		
		return "Review/review-kem-nen-than-thanh-maybelline-fit-me";
	}
	
	@RequestMapping(value = {"/review/review-nuoc-tay-trang-loreal-micellar-water-co-tot-khong"})
	public String review_8 () {
		
		return "Review/review-nuoc-tay-trang-loreal-micellar-water-co-tot-khong";
	}
	
	@RequestMapping(value = {"/bi-quyet-lam-dep/5-mau-son-danh-cho-lua-tuoi-hoc-sinh"})
	public String review_9 () {
		
		return "Bi-quyet-lam-dep/5-mau-son-danh-cho-lua-tuoi-hoc-sinh";
	}

}
