package com.example.demo.resource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.Catalog;
import com.example.demo.model.Product;
import com.example.demo.service.CatalogService;
import com.example.demo.service.ProductService;

@Controller
public class ProductResource {

	private SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private CatalogService catalogService;
	
	@RequestMapping(value = {"/admin/view-product"})
	public String getViewProduct (HttpSession session) {
		
		/*														*/
		/*				CHECK USERNAME ATTR TO SESSION			*/
		/*														*/
		String username = (String) session.getAttribute("username");
		
		if (username == null || !username.equals("admin"))
			return "redirect:/welcome";
		
		return "Admin/QuanLy/san-pham";
	}
	
	@RequestMapping(value = {"/admin/list-product"})
	@ResponseBody
	public List<Product> getAllProduct (HttpSession session) {
		
		/*														*/
		/*				CHECK USERNAME ATTR TO SESSION			*/
		/*														*/
		String username = (String) session.getAttribute("username");
		
		if (username == null || !username.equals("admin"))
			return new ArrayList<>();
		
		return productService.getAllProduct();
	}
	
	@RequestMapping(value = {"/admin/insert-product"}, method = RequestMethod.POST)
	@ResponseBody
	public Product insertProduct (HttpServletRequest req, HttpServletResponse resp) throws ParseException {

		String iD = req.getParameter("iD");
		String iDCatalog = req.getParameter("iDCatalog");
		String fullName = req.getParameter("fullName");
		String description = req.getParameter("description");
		int originPrice = Integer.parseInt(req.getParameter("originPrice"));
		int salePrice = Integer.parseInt(req.getParameter("salePrice"));
		String function = req.getParameter("function");
		int numbers = Integer.parseInt(req.getParameter("numbers"));
		String urlImg = req.getParameter("urlImg");
		String urlImgs = req.getParameter("urlImgs");
		String origin = req.getParameter("origin");
		String trademark = req.getParameter("trademark");
		String shortInfomation = req.getParameter("shortInfomation");
		String longFunc = req.getParameter("longFunc");
		String using = req.getParameter("using");
		Date date = df.parse(req.getParameter("date"));
		
		Product product = new Product(iDCatalog, fullName, description, originPrice, salePrice, function, numbers,
				urlImg, urlImgs, origin, trademark, shortInfomation, longFunc, using, date);
		
		productService.insertProduct(product);
		
		return product;
	}
	
	@RequestMapping(value = {"/admin/edit-product"}, method = RequestMethod.POST)
	@ResponseBody
	public Product editProduct (HttpServletRequest req, HttpServletResponse resp) throws ParseException {
		String iD = req.getParameter("iD");
		String iDCatalog = req.getParameter("iDCatalog");
		String fullName = req.getParameter("fullName");
		String description = req.getParameter("description");
		int originPrice = Integer.parseInt(req.getParameter("originPrice"));
		int salePrice = Integer.parseInt(req.getParameter("salePrice"));
		String function = req.getParameter("function");
		int numbers = Integer.parseInt(req.getParameter("numbers"));
		String urlImg = req.getParameter("urlImg");
		String urlImgs = req.getParameter("urlImgs");
		String origin = req.getParameter("origin");
		String trademark = req.getParameter("trademark");
		String shortInfomation = req.getParameter("shortInfomation");
		String longFunc = req.getParameter("longFunc");
		String using = req.getParameter("using");
		Date date = df.parse(req.getParameter("date"));
		
		Product product = new Product(iD, iDCatalog, fullName, description, originPrice, salePrice, function, numbers,
				urlImg, urlImgs, origin, trademark, shortInfomation, longFunc, using, date);
		
		productService.editProduct(product);
		
		return product;
	}
	
	@RequestMapping(value = {"/admin/delete-product"}, method = RequestMethod.POST)
	@ResponseBody
	public String deleteProduct (HttpServletRequest req, HttpServletResponse resp) {
//		Enumeration<String> parameterNames = req.getParameterNames();
//		String action = req.getParameter(parameterNames.nextElement());
//		
//		String iD = req.getParameter(parameterNames.nextElement());
		
		String iD = req.getParameter("iD");
		
		productService.deleteProduct(iD);
		
		return iD;
	}
	
	@RequestMapping(value = {"/product/get-product"}, params = {"iD", "catalogName", "catalogID"})
	public String getProductByID (@RequestParam("iD") String iD, @RequestParam("catalogName") String catalogName,
								@RequestParam("catalogID") String catalogID, Model model) {
		
		// Details information
		Product pro = productService.getProductByID(iD);
		
		String [] listImgs = new String [0];
		String urlImgs = pro.getUrlImgs();
		
		if (urlImgs != null) 
			listImgs = urlImgs.split(";");		
			
		String [] listFunctions = new String [0];
		String functions = pro.getFunction();
		
		if (functions != null) 
			listFunctions = functions.split(";");
		
		int savePrice = pro.getOriginPrice() - pro.getSalePrice();
		int percentPrice = savePrice * 100 / pro.getOriginPrice();
		
		if (catalogName == null || catalogName.equals("")) {
			Catalog c = catalogService.getCatalogById(catalogID);
			catalogName = c.getName();
		}
		
		// Other products suggest
		List<Product> listOtherType = new ArrayList<>();
		List<Product> listProduct = productService.getAllProduct();
		
		// Same type products suggest
		List<Product> listSameType = new ArrayList<>();		
		
		// List catalog
		List<Catalog> listCatalog = catalogService.getAllCatalog();

		for (Product p:listProduct) {
			
			if (p.getiDCatalog().equals(pro.getiDCatalog())) {
				listSameType.add(p);
			} else {
				listOtherType.add(p);
			}
		}
		
		// List new product
		List<Product> listNewProduct = new ArrayList<>();
		listNewProduct = productService.getListNewProduct();
		
		model.addAttribute("product", pro);
		model.addAttribute("listImgs", listImgs);
		model.addAttribute("catalogName", catalogName);
		model.addAttribute("catalogID", catalogID);
		model.addAttribute("savePrice", savePrice);
		model.addAttribute("percentPrice", percentPrice);
		model.addAttribute("listFunctions", listFunctions);
		model.addAttribute("listSameType", listSameType);
		model.addAttribute("listOtherType", listOtherType);
		model.addAttribute("listCatalog", listCatalog);
		model.addAttribute("listNewProduct", listNewProduct);
		
		return "San-pham/san-pham";
	}
	
}
