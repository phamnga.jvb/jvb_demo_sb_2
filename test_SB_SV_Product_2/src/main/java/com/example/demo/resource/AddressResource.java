package com.example.demo.resource;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.District;
import com.example.demo.model.Region;
import com.example.demo.model.Ward;
import com.example.demo.service.DistrictService;
import com.example.demo.service.RegionService;
import com.example.demo.service.WardService;

@Controller
@RequestMapping(value = "/NoiDungFooter")
public class AddressResource {

	@Autowired
	private RegionService regionService;
	
	@Autowired
	private DistrictService districtService;
	 
	@Autowired
	private WardService wardService;
	
	@RequestMapping(value = "/GetRegion", method = RequestMethod.POST)
	@ResponseBody
	public List<Region> getAllRegion () {
		
		return regionService.getAllRegion();
	}
	
	@RequestMapping(value = "/GetDistrit", method = RequestMethod.POST) 
	@ResponseBody
	public List<District> getAllDistrictByRegionId (HttpServletRequest req) {
		String RegionId = req.getParameter("Id");
		
		return districtService.getAllDistrictByRegionId(RegionId);
	}
	
	@RequestMapping(value = "/GetWard", method = RequestMethod.POST) 
	@ResponseBody
	public List<Ward> getAllWardByDistrictId (HttpServletRequest req) {
		String DistrictId = req.getParameter("Id");
		
		return wardService.getAllWardByDistrictId(DistrictId);
	}
}
