package com.example.demo.resource;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Catalog;
import com.example.demo.model.Order;
import com.example.demo.model.Product;
import com.example.demo.model.Transaction;
import com.example.demo.model.User;
import com.example.demo.service.CatalogService;
import com.example.demo.service.OrderService;
import com.example.demo.service.ProductService;
import com.example.demo.service.TransactionService;
import com.example.demo.service.UserService;

@Controller
public class UserResource {

	@Autowired
	private UserService userService;
	
	@Autowired
	private CatalogService catalogService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private TransactionService transactionService;
	
	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value = {"/", "/welcome"})
	public String welcome (HttpSession session, Model model) {
		
		/*														*/
		/*				SET USERNAME ATTR TO SESSION			*/
		/*														*/
		String username = (String) session.getAttribute("username");
		
		if (username == null || username.equals(""))
			session.setAttribute("username", "guest");
		
		/*								 							*/
		/*			SET LIST-NEW-PRODUCT ATTR TO SESSION		   	*/
		/*															*/
		if (session.getAttribute("listNewProduct") == null) {
			List<Product> listNewProduct = productService.getListNewProduct();
			session.setAttribute("listNewProduct", listNewProduct);
		}
		
		/*												*/
		/*	GET CATALOG CHILD WITH PARENT CATALOG NAME	*/
		/*												*/
		List<Catalog> listMakeupCatalog = new ArrayList<>();
		List<Catalog> listSkinCareCatalog = new ArrayList<>();
		listMakeupCatalog = catalogService.getAllCatalogByNameParent("TRANG ĐIỂM");
		listSkinCareCatalog = catalogService.getAllCatalogByNameParent("CHĂM SÓC DA");
		
		/*															*/
		/*			GET LIST PRODUCT CHILD WITH EACH CATALOG 		*/
		/*															*/
		List<List<Product>> listMakeupProducts = new ArrayList<>();
		List<List<Product>> listSkinCareProducts = new ArrayList<>();
		
		for (Catalog catalog:listMakeupCatalog) {
			List<Product> list = new ArrayList<>();
		
			/*															*/
			/*			GET LIST CATALOG CHILD WITH EACH CATALOG 		*/
			/*															*/
			String makeUpID = catalog.getiD();
			List<Catalog> listCatalogChilds = new ArrayList<>();
			listCatalogChilds.add(catalog);
			listCatalogChilds.addAll(catalogService.getAllCatalogByOriginId(makeUpID));
			
			for (Catalog c:listCatalogChilds) {
				String iDCatalog = c.getiD();
				list.addAll(productService.getListProductByIdCatalog(iDCatalog));
			}
			
			listMakeupProducts.add(list);
		}
		
		for (Catalog catalog:listSkinCareCatalog) {
			List<Product> list = new ArrayList<>();
			
			/*															*/
			/*			GET LIST CATALOG CHILD WITH EACH CATALOG 		*/
			/*															*/
			String skinCareId = catalog.getiD();
			List<Catalog> listCatalogChilds = new ArrayList<>();
			listCatalogChilds.add(catalog);
			listCatalogChilds.addAll(catalogService.getAllCatalogByOriginId(skinCareId));
						
			for (Catalog c:listCatalogChilds) {
				String iDCatalog = c.getiD();
				list.addAll(productService.getListProductByIdCatalog(iDCatalog));
			}
			
			listSkinCareProducts.add(list);
		}
		
		/*								*/
		/*	GET ALL ATTRIBUTES TO MODEL */
		/*								*/
		model.addAttribute("listMakeupCatalog", listMakeupCatalog);
		model.addAttribute("listSkinCareCatalog", listSkinCareCatalog);
		model.addAttribute("listMakeupProducts", listMakeupProducts);
		model.addAttribute("listSkinCareProducts", listSkinCareProducts);
		
		return "index";
	}
	
	@GetMapping("/login")
	public String login (HttpSession session) {
		session.setAttribute("username", "guest");
		
		return "dang-nhap";
	}
	
	@GetMapping("/register")
	public String register () {
		
		return "dang-ky";
	}
	
	@RequestMapping(value = "/checkLogin", method = RequestMethod.POST)
	public ModelAndView checkLogin (@RequestParam ("userName") String userName, @RequestParam ("password") String password,
			HttpServletRequest req, HttpServletResponse res, ModelMap model,
			HttpSession session) {
		res.setContentType("text/html");
		String result = "";

		try {
			PrintWriter writer = res.getWriter();
			res.setContentType("text/html");

			User user = new User();
			user.setUserName(userName);
			user.setPassword(password);

			if (userService.isUser(user)) {
				result = "index";
				session.setAttribute("username", userName);
				
				user = userService.getUserByUserName(userName);
				session.setAttribute("user", user);
				
				// CHECK TRANSACTION OF THIS USER EXISTS OR NOT
				Transaction transaction = transactionService.getOnlyTransactionByIDUser(user.getiD());
				
				// IF TRANSACTION EXISTS
				if (transaction != null) {
					session.setAttribute("transaction", transaction);	
					List<Order> listOrder = orderService.getOrderByIdTransaction(transaction.getiD());
					session.setAttribute("listOrder", listOrder);
					
					OrderResource orderRes = new OrderResource();
					
					int total = orderRes.getTotal(listOrder);
					
					List<Product> listProduct = new ArrayList<>();
					
					for (Order ord: listOrder) {
						String iDProduct2 = ord.getiDProduct();
						Product prd = productService.getProductByID(iDProduct2);
						listProduct.add(prd);
					}
					
					session.setAttribute("listProduct", listProduct);
					session.setAttribute("total", total);
				}
				
				// REMOVE COMPLAIN WHEN LOGINED
				if (model.containsAttribute("complain"))
					model.remove("complain");
			} else {
				session.setAttribute("username", "guest");
				
				// ADD COMPLAIN WHEN LOGIN FAILED
				model.addAttribute("complain", "Mật khẩu / Tên đăng nhập không chính xác!");
				
				return new ModelAndView("dang-nhap");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new ModelAndView("redirect:/welcome");
		
	}
	
	@RequestMapping(value = {"/logout"})
	public ModelAndView logout (HttpSession session) {
		session.setAttribute("username", "guest");
		session.setAttribute("listOrder", new ArrayList<Order>());
		session.setAttribute("listProduct", new ArrayList<Product>());
		session.setAttribute("total", 0);
		
		return new ModelAndView("redirect:/welcome");
	}
	
}
