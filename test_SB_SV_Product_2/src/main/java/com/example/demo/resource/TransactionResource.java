package com.example.demo.resource;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.Order;
import com.example.demo.model.Product;
import com.example.demo.model.Transaction;
import com.example.demo.model.User;
import com.example.demo.service.DistrictService;
import com.example.demo.service.OrderService;
import com.example.demo.service.ProductService;
import com.example.demo.service.RegionService;
import com.example.demo.service.TransactionService;
import com.example.demo.service.UserService;
import com.example.demo.service.WardService;

@Controller
public class TransactionResource {

	@Autowired
	private TransactionService transactionService;
	
	@Autowired
	private ProductService productService;	
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RegionService regionService;
	
	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private WardService wardService;
	
	private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 

	@RequestMapping(value = {"/admin/view-transaction"})
	public String getViewTransaction (HttpSession session) {
		/*														*/
		/*				CHECK USERNAME ATTR TO SESSION			*/
		/*														*/
		String username = (String) session.getAttribute("username");
		
		if (username == null || !username.equals("admin"))
			return "redirect:/welcome";
		
		return "Admin/QuanLy/giao-dich";
	}
	
	@RequestMapping(value = {"/admin/list-transaction"})
	@ResponseBody
	public List<Transaction> getAllTransaction (HttpSession session) {
		/*														*/
		/*				CHECK USERNAME ATTR TO SESSION			*/
		/*														*/
		String username = (String) session.getAttribute("username");
		
		if (username == null || !username.equals("admin"))
			return new ArrayList<>();
		
		return transactionService.getAllTransaction();
	}
	
	@RequestMapping(value = {"/admin/insert-transaction"}, method = RequestMethod.POST)
	@ResponseBody
	public Transaction insertTransaction (HttpServletRequest req, HttpServletResponse resp) {
		Enumeration<String> parameterNames = req.getParameterNames();
		String action = req.getParameter(parameterNames.nextElement());
		
		String iD = req.getParameter(parameterNames.nextElement());
		String status = req.getParameter(parameterNames.nextElement());
		String iDUser = req.getParameter(parameterNames.nextElement());
		String nameUser = req.getParameter(parameterNames.nextElement());
		String phoneUser = req.getParameter(parameterNames.nextElement());
		String emailUser = req.getParameter(parameterNames.nextElement());
		String address = req.getParameter(parameterNames.nextElement());
		String created = req.getParameter(parameterNames.nextElement());
		String amount = req.getParameter(parameterNames.nextElement());
		String content = req.getParameter(parameterNames.nextElement());
		String code = req.getParameter(parameterNames.nextElement());
		String payment = req.getParameter(parameterNames.nextElement());
		String payInfo = req.getParameter(parameterNames.nextElement());

		Transaction transaction = new Transaction(status, iDUser, nameUser, phoneUser,
				emailUser, address, created, amount, content, code, payment, payInfo);
		
		transactionService.insertTransaction(transaction);
		
		return transaction;
	}
	
	@RequestMapping(value = {"/admin/edit-transaction"}, method = RequestMethod.POST)
	@ResponseBody
	public Transaction editTransaction(HttpServletRequest req, HttpServletResponse resp) {
		
		String iD = req.getParameter("iD");
		String status = req.getParameter("status");
		String iDUser = req.getParameter("iduser");
		String nameUser = req.getParameter("nameUser");
		String phoneUser = req.getParameter("phoneUser");
		String emailUser = req.getParameter("emailUser");
		String address = req.getParameter("address");
		String created = req.getParameter("created");
		String amount = req.getParameter("amount");
		String content = req.getParameter("content");
		String code = req.getParameter("code");
		String payment = req.getParameter("payment");
		String payInfo = req.getParameter("payInfo");
		
		Transaction transaction = new Transaction(iD, status, iDUser, nameUser, phoneUser, 
				emailUser, address, created, amount, content, code, payment, payInfo);
		
		transactionService.editTransaction(transaction);
		
		return transaction;
	}
	
	@RequestMapping(value = {"/admin/delete-transaction"}, method = RequestMethod.POST)
	@ResponseBody
	public String deleteProduct (HttpServletRequest req, HttpServletResponse resp) {		
		String iD = req.getParameter("iD");
		
		transactionService.deleteTransaction(iD);
		
		return iD;
	}
	
	@RequestMapping(value = {"/transaction/resolve"})
	public String transactionResolve () {
		
		return "thanh-toan-don-hang";
	}
	
	@RequestMapping(value = {"/transaction/add"}, params = {"iDProduct", "numbers", "status", "comment"})
	public String transactionAdd (HttpServletRequest req, HttpSession session,
			@RequestParam("iDProduct") String iDProduct, @RequestParam("numbers") int numbers, 
			@RequestParam("status") String status, @RequestParam("comment") String comment) throws ParseException {
		
		// CREATE OR GET A TRANSACTION IF IT EXISTS
				Transaction transaction = (Transaction) req.getSession().getAttribute("transaction");
				
				if (transaction == null || transaction.getStatus() != null) {	
					
					String date = df.format(new Date());			
					String userName = (String) session.getAttribute("username");
					User user = userService.getUserByUserName(userName);	
					
					// FIND TRANSACTION IN TABLE TRANSACTION
					transaction = transactionService.getOnlyTransactionByIDUser(user.getiD());
					
					// IF TRANSACTION NOT EXISTS IN TRANSACTION TABLE
					if (transaction == null) {		
						transaction = new Transaction();
						transaction.setIDUser(user.getiD());
						transaction.setNameUser(user.getFullName());
						transaction.setPhoneUser(user.getPhone());
						transaction.setEmailUser(user.getEmail());
						transaction.setCreated(date);
						
						String address = user.getAddress();
						String[] adds = address.split(" - ");
						String finalAdderess = adds[0] + " - " + wardService.getWardById(adds[1]).getWardName() + " - "
								+ districtService.getDistrictById(adds[2]).getDistrictName() + " - "
								+ regionService.getRegionById(adds[3]).getRegionName();
						transaction.setAddress(finalAdderess);
						
						transactionService.insertTransaction(transaction);
						transaction = transactionService.getTransactionByIDUserAndCreateDate(transaction.getIDUser(),
								df.parse(transaction.getCreated()));
					}
					
					session.setAttribute("transaction", transaction);
				}

				// CREATE OR GET A LIST OF ORDER IF IT EXISTS
				List<Order> listOrder = (List<Order>) session.getAttribute("listOrder");
				List<Product> listProduct = new ArrayList<>();
				
				if (listOrder == null) {
					listOrder = new ArrayList<Order>();
				}
				
				Product product = new Product();
				product = productService.getProductByID(iDProduct);
				
				// CHECK PRODUCT EXISTS OR NOT
				for (Order order: listOrder) {
					
					if (order.getiDProduct().equals(iDProduct)) {	// IF EXISTS					
						int oldNumbers = order.getNumbers();
						numbers += oldNumbers;
						int amount = order.getAmount();
						amount = numbers * product.getSalePrice();
						
						Order newOrder = new Order(order.getiD(), transaction.getiD(), iDProduct, numbers, amount, order.getComment(), order.getStatus());
						listOrder.remove(order);
						orderService.editOrder(newOrder);
						listOrder.add(newOrder);
						
						int total = getTotal(listOrder);
						
						for (Order ord: listOrder) {
							String iDProduct2 = ord.getiDProduct();
							Product prd = productService.getProductByID(iDProduct2);
							listProduct.add(prd);
						}
						
						session.setAttribute("listOrder", listOrder);
						session.setAttribute("listProduct", listProduct);
						session.setAttribute("total", total);
						return "thanh-toan-don-hang";
					}
				}
				Order newOrder = new Order(transaction.getiD(), iDProduct, numbers, numbers * product.getSalePrice(), "", "");
				listOrder.add(newOrder);
//				List<Order> listOldOrder = orderService.getOrderByIdTransaction(transaction.getiD());
//				listOrder.addAll(listOldOrder);
				orderService.insertOrder(newOrder);
				
				int total = getTotal(listOrder);
						
				for (Order ord: listOrder) {
					String iDProduct2 = ord.getiDProduct();
					Product prd = productService.getProductByID(iDProduct2);
					listProduct.add(prd);
					
					System.out.println(ord.toString());
				}
				
				session.setAttribute("listOrder", listOrder);
				session.setAttribute("listProduct", listProduct);
				session.setAttribute("total", total);
				
		return "thanh-toan-don-hang";
	}
	
	@RequestMapping(value = {"/transaction/get-resolve"}, method = RequestMethod.POST)
	@ResponseBody
	public String transactionGetResolve (HttpServletRequest req, HttpServletResponse res,
			HttpSession session) {		
		String name = req.getParameter("form[Name]");
		String phone = req.getParameter("form[Phone]");
		String email = req.getParameter("form[Email]");
		String address = req.getParameter("form[Address]");
		String brandId = req.getParameter("form[BrandId]");
		String content = req.getParameter("form[Note]");	
		String region = req.getParameter("form[region]");
		String distric = req.getParameter("form[distric]");
		String ward = req.getParameter("form[ward]");
		
		String amount = String.valueOf(session.getAttribute("total"));
		
//		String source = attributes.get(10);
//		
//		String coupname = attributes.get(11);
//		String priceorder = attributes.get(12);
//		String pricepay = attributes.get(13);
//		String pricecoup = attributes.get(14);
//		String typeship = attributes.get(15);
//		String shipprice = attributes.get(16);
		
		User user = (User) session.getAttribute("user");
		Transaction oldTransaction = (Transaction) session.getAttribute("transaction");
		
		Transaction transaction = new Transaction(oldTransaction.getiD(), "waiting", user.getiD(), name, phone,
				email, address, oldTransaction.getCreated(), amount, content, "", "", "");
		
		transactionService.editTransaction(transaction);
		session.setAttribute("transaction", transaction);
		
		// update session
		List<Order> listOrder = (List<Order>) session.getAttribute("listOrder");
		List<Product> listProduct = (List<Product>) session.getAttribute("listProduct");
		int total = (int) session.getAttribute("total");
		
		if (listOrder != null)
			session.setAttribute("listOrder", new ArrayList<Order>());
		
		if (listProduct != null)
			session.setAttribute("listProduct", new ArrayList<Product>());
		
		if (total != 0)
			session.setAttribute("total", 0);

		return "/dat-hang-thanh-cong";
	}
	
	/*
	@RequestMapping(value = {"/transaction/get-resolve"}, method = RequestMethod.POST)
	@ResponseBody
	public String transactionGetResolve (HttpServletRequest req, HttpServletResponse res,
			HttpSession session) {
		
		Map<String, String[]> a = req.getParameterMap();
		Iterator<String> itr = a.keySet().iterator();
		ArrayList<String> attributes = new ArrayList<>();
		int i = 0;
		
		while (itr.hasNext()) {
			String key = itr.next();
			attributes.add(a.get(key)[0]);
		}
		
		String name = attributes.get(0);
		String phone = attributes.get(1);
		String email = attributes.get(2);
		String address = attributes.get(3);
		String brandId = attributes.get(4);
		String note = attributes.get(5);	
		String brandname = attributes.get(6);		
		String region = attributes.get(7);
		String distric = attributes.get(8);
		String ward = attributes.get(9);
		
		String source = attributes.get(10);
		
		String coupname = attributes.get(11);
		String priceorder = attributes.get(12);
		String pricepay = attributes.get(13);
		String pricecoup = attributes.get(14);
		String typeship = attributes.get(15);
		String shipprice = attributes.get(16);
		
		User user = (User) session.getAttribute("user");
		Transaction oldTransaction = (Transaction) session.getAttribute("transaction");
		
		Transaction transaction = new Transaction(oldTransaction.getiD(), "waiting", user.getiD(), name, phone,
				email, address, oldTransaction.getCreated(), pricepay, note, "", "", "");
		
		transactionService.editTransaction(transaction);
		session.setAttribute("transaction", transaction);
		
		// update session
		List<Order> listOrder = (List<Order>) session.getAttribute("listOrder");
		List<Product> listProduct = (List<Product>) session.getAttribute("listProduct");
		int total = (int) session.getAttribute("total");
		
		if (listOrder != null)
			session.setAttribute("listOrder", new ArrayList<Order>());
		
		if (listProduct != null)
			session.setAttribute("listProduct", new ArrayList<Product>());
		
		if (total != 0)
			session.setAttribute("total", 0);

		return "/dat-hang-thanh-cong";
	}
	*/
	
	@RequestMapping(value = {"/dat-hang-thanh-cong"})
	public String success () {
		
		return "dat-hang-thanh-cong";
	}
	
	public int getTotal (List<Order> listOrder) {
		int total = 0;
		
		for (Order order:listOrder) 
			total += order.getAmount();
		
		return total;
	}
	
}
