package com.example.demo.resource;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.Order;
import com.example.demo.model.Product;
import com.example.demo.model.Transaction;
import com.example.demo.model.User;
import com.example.demo.service.DistrictService;
import com.example.demo.service.OrderService;
import com.example.demo.service.ProductService;
import com.example.demo.service.RegionService;
import com.example.demo.service.TransactionService;
import com.example.demo.service.UserService;
import com.example.demo.service.WardService;

@Controller
public class OrderResource {

	@Autowired
	private OrderService orderService;
	
	@Autowired
	private TransactionService transactionService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ProductService productService;	
	
	@Autowired
	private RegionService regionService;
	
	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private WardService wardService;
	
	private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
	
	@RequestMapping(value = {"/admin/view-order"})
	public String getViewOrder (HttpSession session) {
		
		/*														*/
		/*				CHECK USERNAME ATTR TO SESSION			*/
		/*														*/
		String username = (String) session.getAttribute("username");
		
		if (username == null || !username.equals("admin"))
			return "redirect:/welcome";
		
		return "Admin/QuanLy/don-hang";
	}
	
	@RequestMapping(value = {"/admin/list-order"})
	@ResponseBody
	public List<Order> getAllOrder (HttpSession session) {
		
		/*														*/
		/*				CHECK USERNAME ATTR TO SESSION			*/
		/*														*/
		String username = (String) session.getAttribute("username");
		
		if (username == null || !username.equals("admin"))
			return new ArrayList<>();
		
		return orderService.getAllOrder();
	}
	
	@RequestMapping(value = {"/admin/insert-order"}, method = RequestMethod.POST)
	@ResponseBody
	public Order insertOrder (HttpServletRequest req, HttpServletResponse resp) {
		Enumeration<String> parameterNames = req.getParameterNames();
		String action = req.getParameter(parameterNames.nextElement());
		
		String iD = req.getParameter(parameterNames.nextElement());
		String iDTransaction = req.getParameter(parameterNames.nextElement());
		String iDProduct = req.getParameter(parameterNames.nextElement());
		int numbers = Integer.parseInt(req.getParameter(parameterNames.nextElement()));
		int amount = Integer.parseInt(req.getParameter(parameterNames.nextElement()));
		String comment = req.getParameter(parameterNames.nextElement());
		String status = req.getParameter(parameterNames.nextElement());
		
		Order order = new Order(iDTransaction, iDProduct, numbers, amount, comment, status);
		
		orderService.insertOrder(order);
		
		return order;
	}
	
	@RequestMapping(value = {"/admin/edit-order"}, method = RequestMethod.POST)
	@ResponseBody
	public Order editOrder(HttpServletRequest req, HttpServletResponse resp) {
		
		try {
			req.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Enumeration<String> parameterNames = req.getParameterNames();
		String action = req.getParameter(parameterNames.nextElement());
		
		String iD = req.getParameter(parameterNames.nextElement());
		String iDTransaction = req.getParameter(parameterNames.nextElement());
		String iDProduct = req.getParameter(parameterNames.nextElement());
		int numbers = Integer.parseInt(req.getParameter(parameterNames.nextElement()));
		int amount = Integer.parseInt(req.getParameter(parameterNames.nextElement()));
		String comment = req.getParameter(parameterNames.nextElement());
		String status = req.getParameter(parameterNames.nextElement());
		
		Order order = new Order(iD, iDTransaction, iDProduct, numbers, amount, comment, status);
		
		orderService.editOrder(order);
		
		return order;
	}
	
	@RequestMapping(value = {"/admin/delete-order"}, method = RequestMethod.POST)
	@ResponseBody
	public String deleteProduct (HttpServletRequest req, HttpServletResponse resp) {		
		String iD = req.getParameter("iD");
		
		orderService.deleteOrder(iD);
		
		return iD;
	}
	
	@RequestMapping(value = {"/order/add"}, params = {"iDProduct", "numbers", "status", "comment"})
	public String addOrder (HttpServletRequest req, HttpSession session, 
			@RequestParam("iDProduct") String iDProduct, @RequestParam("numbers") int numbers, 
			@RequestParam("status") String status, @RequestParam("comment") String comment) throws ParseException {
		
		// CREATE OR GET A TRANSACTION IF IT EXISTS
		Transaction transaction = (Transaction) req.getSession().getAttribute("transaction");
		
		if (transaction == null || transaction.getStatus() != null) {	
			
			String date = df.format(new Date());			
			String userName = (String) session.getAttribute("username");
			User user = userService.getUserByUserName(userName);	
			
			// FIND TRANSACTION IN TABLE TRANSACTION
			transaction = transactionService.getOnlyTransactionByIDUser(user.getiD());
			
			// IF TRANSACTION NOT EXISTS IN TRANSACTION TABLE
			if (transaction == null) {		
				transaction = new Transaction();
				transaction.setIDUser(user.getiD());
				transaction.setNameUser(user.getFullName());
				transaction.setPhoneUser(user.getPhone());
				transaction.setEmailUser(user.getEmail());
				transaction.setCreated(date);
				
				String address = user.getAddress();
				String[] adds = address.split(" - ");
				String finalAdderess = adds[0] + " - " + wardService.getWardById(adds[1]).getWardName() + " - "
						+ districtService.getDistrictById(adds[2]).getDistrictName() + " - "
						+ regionService.getRegionById(adds[3]).getRegionName();
				transaction.setAddress(finalAdderess);
				
				transactionService.insertTransaction(transaction);
				transaction = transactionService.getTransactionByIDUserAndCreateDate(transaction.getIDUser(),
						df.parse(transaction.getCreated()));
			}
			
			session.setAttribute("transaction", transaction);
		}

		// CREATE OR GET A LIST OF ORDER IF IT EXISTS
		List<Order> listOrder = (List<Order>) session.getAttribute("listOrder");
		List<Product> listProduct = new ArrayList<>();
		
		if (listOrder == null) {
			listOrder = new ArrayList<Order>();
		}
		
		Product product = new Product();
		product = productService.getProductByID(iDProduct);
		
		// CHECK PRODUCT EXISTS OR NOT
		for (Order order: listOrder) {
			
			if (order.getiDProduct().equals(iDProduct)) {	// IF EXISTS					
				int oldNumbers = order.getNumbers();
				numbers += oldNumbers;
				int amount = order.getAmount();
				amount = numbers * product.getSalePrice();
				
				Order newOrder = new Order(order.getiD(), transaction.getiD(), iDProduct, numbers, amount, order.getComment(), order.getStatus());
				listOrder.remove(order);
				orderService.editOrder(newOrder);
				listOrder.add(newOrder);
				
				int total = getTotal(listOrder);
				
				for (Order ord: listOrder) {
					String iDProduct2 = ord.getiDProduct();
					Product prd = productService.getProductByID(iDProduct2);
					listProduct.add(prd);
				}
				
				session.setAttribute("listOrder", listOrder);
				session.setAttribute("listProduct", listProduct);
				session.setAttribute("total", total);
				
				return "gio-hang";
			}
		}
		Order newOrder = new Order(transaction.getiD(), iDProduct, numbers, numbers * product.getSalePrice(), "", "");
		listOrder.add(newOrder);
//		List<Order> listOldOrder = orderService.getOrderByIdTransaction(transaction.getiD());
//		listOrder.addAll(listOldOrder);
		orderService.insertOrder(newOrder);
		
		int total = getTotal(listOrder);
				
		for (Order ord: listOrder) {
			String iDProduct2 = ord.getiDProduct();
			Product prd = productService.getProductByID(iDProduct2);
			listProduct.add(prd);
			
			System.out.println(ord.toString());
		}
		
		session.setAttribute("listOrder", listOrder);
		session.setAttribute("listProduct", listProduct);
		session.setAttribute("total", total);
		
		return "gio-hang";
	}
	
	@RequestMapping(value = {"/order/remove"}, params = {"iDProduct"})
	public String removeProductInOrder (HttpSession session, @RequestParam("iDProduct") String iDProduct) {
		List<Order> listOrder = (List<Order>) session.getAttribute("listOrder");
		List<Order> listProduct = (List<Order>) session.getAttribute("listProduct");
		
		for (int i = 0; i < listOrder.size(); i++) {
		
			if (listOrder.get(i).getiDProduct().equals(iDProduct)) {
				
				listOrder.remove(i);
				listProduct.remove(i);
				
				int total = getTotal(listOrder);
				
				session.setAttribute("listOrder", listOrder);
				session.setAttribute("listProduct", listProduct);
				session.setAttribute("total", total);
				
				break;
			}
		}
		
		return "gio-hang";
	}
	
	@RequestMapping(value = {"/order/view"})
	public String viewOrder () {
				
		return "gio-hang";
	}
	
	public int getTotal (List<Order> listOrder) {
		int total = 0;
		
		for (Order order:listOrder) 
			total += order.getAmount();
		
		return total;
	}
	
}
