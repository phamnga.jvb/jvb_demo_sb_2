package com.example.demo.resource;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.Catalog;
import com.example.demo.model.Product;
import com.example.demo.service.CatalogService;
import com.example.demo.service.ProductService;

@Controller
public class CatalogResource {

	@Autowired
	private CatalogService catalogService;
	private List<Catalog> listCatalog;
	
	@Autowired
	private ProductService productService;
	
	/**
	 * 
	 */
	public CatalogResource() {
		this.catalogService = new CatalogService();
		this.listCatalog = new ArrayList<>();
		this.productService = new ProductService();
	}

	@RequestMapping(value = {"/admin/view-catalog"})
	public String getViewCatalog (HttpSession session, Model model) {
		
		/*														*/
		/*				CHECK USERNAME ATTR TO SESSION			*/
		/*														*/
		String username = (String) session.getAttribute("username");
		
		if (username == null || !username.equals("admin"))
			return "redirect:/welcome";
		
		listCatalog = catalogService.getAllCatalog();
		List<String> listIDCatalog = new ArrayList<>();

		for (Catalog c:listCatalog) {
			listIDCatalog.add(c.getiD());
		}
		model.addAttribute("listIDCatalog", listIDCatalog);
		
		return "Admin/QuanLy/danh-muc";
	}
	
	@RequestMapping(value = {"/admin/list-catalog"})
	@ResponseBody
	public List<Catalog> getAllCatalog (HttpSession session, Model model) {
		
		/*														*/
		/*				CHECK USERNAME ATTR TO SESSION			*/
		/*														*/
		String username = (String) session.getAttribute("username");
		
		if (username == null || !username.equals("admin"))
			return new ArrayList<>();
		
		listCatalog = catalogService.getAllCatalog();
//		model.addAttribute("listCatalog", listCatalog);
		List<String> listIDCatalog = new ArrayList<>();
		
		for (Catalog c:listCatalog) {
			listIDCatalog.add(c.getiD());
		}
		model.addAttribute("listIDCatalog", listIDCatalog);
		
		return listCatalog;
	}
	
	@RequestMapping(value = {"/admin/edit-catalog"}, method = RequestMethod.POST)
	@ResponseBody
	public Catalog editCatalog (HttpServletRequest req, HttpServletResponse resp) {
		resp.setContentType("application/json");
				
		String iD = req.getParameter("iD");
		String parentID = req.getParameter("parentID");
		String name = req.getParameter("name");
		String des = req.getParameter("des");
		String urlImg = req.getParameter("urlImg");
		
		Catalog catalog = new Catalog(iD, parentID, name, des, urlImg);
		System.out.println(catalog.toString());
		catalogService.editCatalog(catalog);
		
		listCatalog = catalogService.getAllCatalog();
		
		return catalog;
	}
	
	@RequestMapping(value = {"/admin/insert-catalog"}, method = RequestMethod.POST)
	@ResponseBody
	public Catalog insertCatalog (HttpServletRequest req, HttpServletResponse resp) {
		String iD = req.getParameter("iD");
		String parentID = req.getParameter("parentID");
		String name = req.getParameter("name");
		String des = req.getParameter("des");
		String urlImg = req.getParameter("urlImg");
		
		Catalog catalog = new Catalog(iD, parentID, name, des, urlImg);
		System.out.println(catalog.toString());
		
		if (iD != null && iD != "") {		
			catalogService.insertCatalogFull(catalog);
		} else {
			catalogService.insertCatalog(catalog);
		}
		
		listCatalog = catalogService.getAllCatalog();		
		
		return catalog;
	}
	
	@RequestMapping(value = {"/admin/delete-catalog"}, method = RequestMethod.POST)
	@ResponseBody
	public String deleteCatalog (HttpServletRequest req, HttpServletResponse resp) {
		String iD = req.getParameter("iD");

		catalogService.deleteCatalog(iD);
		
		listCatalog = catalogService.getAllCatalog();		
		
		return iD;
	}
	
	@RequestMapping(value = {"/all-catalog"})
	@ResponseBody
	public List<Catalog> getAllCatalog2 () {
		listCatalog = catalogService.getAllCatalog();
		
		return listCatalog;
	}
	
	@RequestMapping(value = {"/getParentCatalog"}, method = RequestMethod.GET)
	@ResponseBody
	public List<Catalog> getParentCatalog () {
		
		return catalogService.getAllCatalogByIdParent("0");
	}
	
	@RequestMapping(value = {"/getChildCatalog"}, method = RequestMethod.GET)
	@ResponseBody
	public List<Catalog> getChildCatalog (HttpServletRequest req, HttpServletResponse resp) {
		String parentId = req.getParameter("parentID");		
		
		return catalogService.getAllCatalogByIdParent(parentId);
	}
	
	@RequestMapping(value = {"/view-catalog"}, params = {"iDCatalog", "nameCatalog"})
	public String viewDetailCatalog (HttpServletRequest req, HttpServletResponse resp,
			@RequestParam("iDCatalog") String iDCatalog, @RequestParam("nameCatalog") String nameCatalog, Model model) {
		
		String title = nameCatalog + " hiệu quả bằng mỹ phẩm chính hãng từ thiên nhiên";
		String des = "";
		
		model.addAttribute("iD", iDCatalog);
		model.addAttribute("name", nameCatalog);
		model.addAttribute("title", title);
				
		for (Catalog c:listCatalog) {
									
			if (c.getiD().equals(iDCatalog)) {
				des = c.getDes();
								
				break;
			}
						
		}
		
		/*												*/
		/*		GET LIST PRODUCT OF THIS CATALOG		*/
		/*												*/
		List<Product> listProductByCatalog = productService.getListProductByIdCatalog(iDCatalog);

		/*														*/
		/*		GET LIST PRODUCT OF THIS CATALOG'S CHILD		*/
		/*														*/
		List<Catalog> listChildCatalog = catalogService.getAllCatalogByOriginId(iDCatalog);
		
		for (Catalog catalog: listChildCatalog) {
			listProductByCatalog.addAll(productService.getListProductByIdCatalog(catalog.getiD()));
		}
		
		// List new product
		List<Product> listNewProduct = new ArrayList<>();
		listNewProduct = productService.getListNewProduct();
		
		model.addAttribute("des", des);
		model.addAttribute("listCatalog", listCatalog);		
		model.addAttribute("listProductByCatalog", listProductByCatalog);
		model.addAttribute("listNewProduct", listNewProduct);

		return "Danh-muc/danh-muc";
	}
	
}
