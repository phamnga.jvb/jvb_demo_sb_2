package com.example.demo.resource;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.User;
import com.example.demo.service.OrderService;
import com.example.demo.service.TransactionService;
import com.example.demo.service.UserService;

@Controller
@RequestMapping("/admin")
public class UserForAdResource {

	@Autowired
	private UserService userService;
	
	@Autowired
	private TransactionService transactionService;
	
	@Autowired
	private OrderService orderService;
	
	/**
	 * @author PN
	 * Return View user.html
	 * @return
	 * @since 03/04/2019
	 */
	@RequestMapping(value = {"/view-user"})
	public String getViewUser (HttpSession session) {
		
		/*														*/
		/*				CHECK USERNAME ATTR TO SESSION			*/
		/*														*/
		String username = (String) session.getAttribute("username");
		
		if (username == null || !username.equals("admin"))
			return "redirect:/welcome";
		
		return "Admin/QuanLy/user";
	}
	
	/**
	 * Get List User
	 * @return
	 */
	@RequestMapping(value = {"/list-user"})
	@ResponseBody
	public List<User> getAllUser (HttpSession session) {
		
		/*														*/
		/*				CHECK USERNAME ATTR TO SESSION			*/
		/*														*/
		String username = (String) session.getAttribute("username");
		
		if (username == null || !username.equals("admin"))
			return new ArrayList<>();
		
		List<User> listUser = userService.getAllUser();
		
		return listUser;		
	}
	
	/**
	 * Delete User
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping(value = {"/delete-user"}, method = RequestMethod.POST)
	public String deleteUser (HttpServletRequest req, HttpServletResponse resp) {
		String iD = req.getParameter("iD");
		List<String> iDTransaction = transactionService.getAllTransactionByUserID(iD);
		
		for (String id: iDTransaction) {
			orderService.deleteOrderByIdTransaction(id);
			transactionService.deleteTransaction(id);
		}
		
		userService.deleteUser(iD);
		
		return "Admin/QuanLy/user";
	}
	
}
