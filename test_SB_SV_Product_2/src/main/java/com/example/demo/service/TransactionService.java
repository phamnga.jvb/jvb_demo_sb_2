package com.example.demo.service;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.example.demo.mapper.TransactionMapper;
import com.example.demo.model.Transaction;

@Service
public class TransactionService {

	@Resource
	private TransactionMapper transactionMapper;
	
	public List<Transaction> getAllTransaction () {
		
		return transactionMapper.getAllTransaction();
	}
	
	public void insertTransaction (Transaction Transaction) {
		transactionMapper.insertTransaction(Transaction);
	}
	
	public void editTransaction (Transaction Transaction) {
		transactionMapper.editTransaction(Transaction);
	}
	
	public void deleteTransaction (String iD) {
		transactionMapper.deleteTransaction(iD);
	}

	public Transaction getTransactionByIDUserAndCreateDate (String iDUser, Date created) {
		
		return transactionMapper.getTransactionByIDUserAndCreateDate(iDUser, created);
	}
	
	public List<String> getAllTransactionByUserID (String iDUser) {
		
		return transactionMapper.getAllTransactionByUserID(iDUser);
	}
	
	public Transaction getOnlyTransactionByIDUser (String iDUser) {
		
		return transactionMapper.getOnlyTransactionByIDUser(iDUser);
	}
}
