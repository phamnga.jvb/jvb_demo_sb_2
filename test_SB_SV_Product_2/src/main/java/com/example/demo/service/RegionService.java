package com.example.demo.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.example.demo.mapper.RegionMapper;
import com.example.demo.model.Region;

@Service
public class RegionService {

	@Resource
	private RegionMapper regionMapper;
	
	public List<Region> getAllRegion () {
		
		return regionMapper.getAllRegion();
	}

	public Region getRegionById (String iD) {
		
		return regionMapper.getRegionById(iD);
	}
}
