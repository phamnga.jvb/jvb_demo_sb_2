package com.example.demo.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.example.demo.mapper.WardMapper;
import com.example.demo.model.Ward;

@Service
public class WardService {

	@Resource
	private WardMapper wardMapper;
	
	public List<Ward> getAllWard () {
		
		return wardMapper.getAllWard();
	}
	
	public List<Ward> getAllWardByDistrictId (String DistrictId) {
		
		return wardMapper.getAllWardByDistrictId(DistrictId);
	}
	
	public Ward getWardById (String iD) {
		
		return wardMapper.getWardById(iD);
	}
}
