package com.example.demo.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.example.demo.mapper.ProductMapper;
import com.example.demo.model.Product;

@Service
public class ProductService {

	@Resource
	private ProductMapper productMapper;
	
	public List<Product> getAllProduct () {
		
		return productMapper.getAllProduct();
	}
	
	public void insertProduct (Product product) {
		productMapper.insertProduct(product);
	}
	
	public void editProduct (Product product) {
		productMapper.editProduct(product);
	}
	
	public void deleteProduct (String iD) {
		productMapper.deleteProduct(iD);
	}
	
	public List<Product> getListProductByIdCatalog (String iDCatalog) {
		
		return productMapper.getListProductByIdCatalog(iDCatalog);
	}
	
	public Product getProductByID (String iD) {
		
		return productMapper.getProductByID(iD);
	}
	
	public List<Product> getListNewProduct () {
		
		return productMapper.getListNewProduct();
	}
	
	public List<Product> getListProductByAString (String aString) {
		aString = "%" + aString.toLowerCase() + "%";
		
		return productMapper.getListProductByAString(aString);
	}
	
}
