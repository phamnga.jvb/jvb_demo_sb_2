package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.example.demo.mapper.CatalogMapper;
import com.example.demo.model.Catalog;

@Service
public class CatalogService {

	@Resource
	private CatalogMapper catalogMapper;
	
	public List<Catalog> getAllCatalog () {
		
		return catalogMapper.getAllCatalog();
	}
	
	public void insertCatalog (Catalog catalog) {		
		catalogMapper.insertCatalog(catalog);
	}
	
	public void insertCatalogFull (Catalog catalog) {		
		catalogMapper.insertCatalogFull(catalog);
	}
	
	public void editCatalog (Catalog catalog) {
		catalogMapper.editCatalog(catalog);
	}
	
	public void deleteCatalog (String iD) {
		catalogMapper.deleteCatalog(iD);
	}
	
	public List<Catalog> getAllCatalogByIdParent (String parentId) {
		
		return catalogMapper.getAllCatalogByIdParent(parentId);
	}
	
	public Catalog getCatalogById (String iD) {
		
		return catalogMapper.getCatalogById(iD);
	}
	
	public List<Catalog> getAllCatalogByNameParent (String parentName) {
		
		return catalogMapper.getAllCatalogByNameParent(parentName);
	}
	
	public List<Catalog> getAllCatalogByOriginId (String iDOrigin) {
		
		List<Catalog> listChilds = new ArrayList<>();
		
		listChilds = getAllCatalogByOriginId2(iDOrigin);
		
		return listChilds;
	}
	
	public List<Catalog> getAllCatalogByOriginId2 (String iDOrigin) {
		List<Catalog> listChild = new ArrayList<>();
						
		if (catalogMapper.getAllCatalogByIdParent(iDOrigin) == null || catalogMapper.getAllCatalogByIdParent(iDOrigin).size() == 0) 
			return listChild;
				
		listChild = catalogMapper.getAllCatalogByIdParent(iDOrigin);
		int sizeOrigin = listChild.size();
		
		for (int i = 0; i < sizeOrigin; i++) {
			Catalog catalog = listChild.get(i);
			listChild.addAll(getAllCatalogByOriginId2(catalog.getiD()));
		}

		return listChild;		
	}
	
}
