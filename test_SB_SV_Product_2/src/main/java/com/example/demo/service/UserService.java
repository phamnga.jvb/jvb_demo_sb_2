package com.example.demo.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.example.demo.mapper.UserMapper;
import com.example.demo.model.User;

@Service
public class UserService {

	@Resource
	private UserMapper userMapper;
	
	public List<User> getAllUser () {
		
		return userMapper.getAllUser();
	}
	
	public boolean isUser (User user) {
		
		User newUser = userMapper.checkUser(user);
		
		if (newUser != null)
			return true;
		
		return false;	
	}
	
	public void insertUser (User user) {
		userMapper.insertUser(user);;
	}
	
	public void deleteUser (String iD) {
		userMapper.deleteUser(iD);
	}
	
	public User getUserByUserName (String userName) {
		
		return userMapper.getUserByUserName(userName);
	}
	
}
