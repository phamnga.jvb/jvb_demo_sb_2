package com.example.demo.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.example.demo.mapper.AdminMapper;
import com.example.demo.model.Admin;

@Service
public class AdminService {

	@Resource
	private AdminMapper adminMapper;
	
	public List<Admin> getAllAdmin() {
		
		return adminMapper.getAllAdmin();
	}
	
	public boolean isAdmin(Admin admin) {
		
		Admin newAdmin = adminMapper.checkAdmin(admin);
				
		if (newAdmin != null) 
			return true;
		
		return false;
	}
	
}
