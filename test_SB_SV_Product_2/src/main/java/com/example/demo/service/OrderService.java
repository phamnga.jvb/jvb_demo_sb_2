package com.example.demo.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.example.demo.mapper.OrderMapper;
import com.example.demo.model.Order;

@Service
public class OrderService {

	@Resource
	private OrderMapper orderMapper;
	
	public List<Order> getAllOrder () {
		
		return orderMapper.getAllOrder();
	}
	
	public void insertOrder (Order order) {
		orderMapper.insertOrder(order);
	}
	
	public void editOrder (Order order) {
		orderMapper.editOrder(order);
	}
	
	public void deleteOrder (String iD) {
		orderMapper.deleteOrder(iD);
	}
	
	public void deleteOrderByIdTransaction (String iDTransaction) {
		orderMapper.deleteOrderByIdTransaction(iDTransaction);
	}
	
	public List<Order> getOrderByIdTransaction (String iDTransaction) {
		
		return orderMapper.getOrderByIdTransaction(iDTransaction);
	}
}
