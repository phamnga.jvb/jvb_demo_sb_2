package com.example.demo.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.example.demo.mapper.DistrictMapper;
import com.example.demo.model.District;

@Service
public class DistrictService {

	@Resource
	private DistrictMapper districtMapper;
	
	public List<District> getAllDistrict () {
		
		return districtMapper.getAllDistrict();
	}
	
	public List<District> getAllDistrictByRegionId (String RegionId) {
		
		return districtMapper.getAllDistrictByRegionId(RegionId);
	}
	
	public District getDistrictById (String iD) {
		
		return districtMapper.getDistrictById(iD);
	}
}
