package com.example.demo.mapper;

import java.util.List;

import com.example.demo.model.District;

public interface DistrictMapper {

	public List<District> getAllDistrict ();
	
	public List<District> getAllDistrictByRegionId (String RegionId);
	
	public District getDistrictById (String iD);
}
