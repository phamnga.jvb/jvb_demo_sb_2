package com.example.demo.mapper;

import java.util.List;

import com.example.demo.model.Catalog;

public interface CatalogMapper {

	public List<Catalog> getAllCatalog ();
	
	public void insertCatalog (Catalog catalog);
	
	public void insertCatalogFull (Catalog catalog);
	
	public void editCatalog (Catalog catalog);
	
	public void deleteCatalog (String iD);
	
	public List<Catalog> getAllCatalogByIdParent (String parentId);
	
	public Catalog getCatalogById (String iD);
	
	public List<Catalog> getAllCatalogByNameParent (String parentName);
}
