package com.example.demo.mapper;

import java.util.List;

import com.example.demo.model.Order;

public interface OrderMapper {

	public List<Order> getAllOrder ();
	
	public void insertOrder (Order order);
	
	public void editOrder (Order order);
	
	public void deleteOrder (String iD);

	public void deleteOrderByIdTransaction (String iDTransaction);
	
	public List<Order> getOrderByIdTransaction (String iDTransaction);
}
