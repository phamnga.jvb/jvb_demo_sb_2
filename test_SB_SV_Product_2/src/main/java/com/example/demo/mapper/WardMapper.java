package com.example.demo.mapper;

import java.util.List;

import com.example.demo.model.Ward;

public interface WardMapper {

	public List<Ward> getAllWard ();
	
	public List<Ward> getAllWardByDistrictId (String DistrictId);

	public Ward getWardById (String iD);
}
