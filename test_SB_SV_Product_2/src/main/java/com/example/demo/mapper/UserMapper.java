package com.example.demo.mapper;

import java.util.List;

import com.example.demo.model.User;

public interface UserMapper {

	public List<User> getAllUser ();
	
	public User checkUser (User user);

	public void insertUser (User user);
	
	public void deleteUser (String iD);
	
	public User getUserByUserName (String userName);
}
