package com.example.demo.mapper;

import java.util.List;

import com.example.demo.model.Admin;

public interface AdminMapper {

	public List<Admin> getAllAdmin();
	
	public Admin checkAdmin(Admin admin);
	
}
