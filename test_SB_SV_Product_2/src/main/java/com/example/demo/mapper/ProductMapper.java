package com.example.demo.mapper;

import java.util.List;

import com.example.demo.model.Product;

public interface ProductMapper {

	public List<Product> getAllProduct ();
	
	public void insertProduct (Product product);
	
	public void editProduct (Product product);
	
	public void deleteProduct (String iD);
	
	public List<Product> getListProductByIdCatalog (String iDCatalog);
	
	public Product getProductByID (String iD);
	
	public List<Product> getListNewProduct ();
	
	public List<Product> getListProductByAString (String aString);
}
