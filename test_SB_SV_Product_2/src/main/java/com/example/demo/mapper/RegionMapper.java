package com.example.demo.mapper;

import java.util.List;

import com.example.demo.model.Region;

public interface RegionMapper {

	public List<Region> getAllRegion ();
	
	public Region getRegionById (String iD);
	
}
