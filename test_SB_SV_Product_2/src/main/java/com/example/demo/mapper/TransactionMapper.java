package com.example.demo.mapper;

import java.util.Date;
import java.util.List;

import com.example.demo.model.Transaction;

public interface TransactionMapper {

	public List<Transaction> getAllTransaction ();
	
	public void insertTransaction (Transaction Transaction);
	
	public void editTransaction (Transaction Transaction);
	
	public void deleteTransaction (String iD);
	
	public Transaction getTransactionByIDUserAndCreateDate (String iDUser, Date created);
	
	public List<String> getAllTransactionByUserID (String idUser);
	
	public Transaction getOnlyTransactionByIDUser (String idUser);
}
