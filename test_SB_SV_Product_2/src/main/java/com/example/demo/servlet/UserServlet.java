package com.example.demo.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.model.User;
import com.example.demo.service.UserService;

@WebServlet(urlPatterns = "/user/checkRegister", loadOnStartup = 1)
public class UserServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private UserService userService;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		
		Map<String, String[]> a = req.getParameterMap();
		Iterator<String> itr = a.keySet().iterator();
		ArrayList<String> attributes = new ArrayList<>();
		
		while (itr.hasNext()) {
			String key = itr.next();
			attributes.add(a.get(key)[0]);
		}
		
		String email = attributes.get(1);
		String name = attributes.get(3);
		String userName = attributes.get(4);
		String phones = attributes.get(0);
		String password = attributes.get(2);
		String region = attributes.get(6);
		String distric = attributes.get(7);
		String ward = attributes.get(8);
		String address = attributes.get(5);
		String addressFin = address + " - " + ward + " - " + distric + " - " + region;
		
		User user = new User(userName, password, email, name, " ", addressFin, phones);
		System.out.println(user.toString());
		
		userService.insertUser(user);
		
		// If insert sucess => return main page
		HttpSession session = req.getSession();
		session.setAttribute("username", userName);
		session.setAttribute("user", user);
		req.getRequestDispatcher("/welcome").forward(req, resp);
		out.println("Success!");
		out.close();
		
		return;
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
	
}
