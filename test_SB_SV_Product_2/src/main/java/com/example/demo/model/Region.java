package com.example.demo.model;

/**
 * This class is about 64 Province in Viet Nam
 * @author Admin
 *
 */
public class Region {

	private String iD;
	private String regionName;
	private String regionType;
	
	public Region(String id, String regionName, String regionType) {
		super();
		this.iD = id;
		this.regionName = regionName;
		this.regionType = regionType;
	}

	public Region() {
		super();
	}

	public String getId() {
		return iD;
	}

	public void setId(String id) {
		iD = id;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getRegionType() {
		return regionType;
	}

	public void setRegionType(String regionType) {
		this.regionType = regionType;
	}	
	
}
