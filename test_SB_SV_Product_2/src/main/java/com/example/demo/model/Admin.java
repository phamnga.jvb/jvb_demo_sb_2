package com.example.demo.model;

public class Admin {

	private int iD;
	private String userName;
	private String password;
	private String fullName;
	private String phone;
	
	public Admin(int iD, String userName, String password, String fullName, String phone) {
		super();
		this.iD = iD;
		this.userName = userName;
		this.password = password;
		this.fullName = fullName;
		this.phone = phone;
	}

	public Admin() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getiD() {
		return iD;
	}

	public void setiD(int iD) {
		this.iD = iD;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Admin [iD=" + iD + ", userName=" + userName + ", password=" + password + ", fullName=" + fullName
				+ ", phone=" + phone + "]";
	}

}
