package com.example.demo.model;

import java.util.Date;

public class Product {

	private String iD;
	private String iDCatalog;
	private String fullName;
	private String description;
	private int originPrice;
	private int salePrice;
	private String function;
	private int numbers;
	private String urlImg;
	private String urlImgs;
	private String origin;
	private String trademark;
	private String shortInfomation;
	private String longFunc;
	private String using;
	private Date date;
	
	/**
	 * 
	 */
	public Product() {
		super();
	}

	/**
	 * @param iD
	 * @param iDCatalog
	 * @param fullName
	 * @param description
	 * @param originPrice
	 * @param salePrice
	 * @param function
	 * @param numbers
	 * @param urlImg
	 * @param urlImgs
	 * @param origin
	 * @param trademark
	 * @param shortInfomation
	 * @param longFunc
	 * @param using
	 */
	public Product(String iD, String iDCatalog, String fullName, String description, int originPrice,
			int salePrice, String function, int numbers, String urlImg, String urlImgs, String origin,
			String trademark, String shortInfomation, String longFunc, String using, Date date) {
		super();
		this.iD = iD;
		this.iDCatalog = iDCatalog;
		this.fullName = fullName;
		this.description = description;
		this.originPrice = originPrice;
		this.salePrice = salePrice;
		this.function = function;
		this.numbers = numbers;
		this.urlImg = urlImg;
		this.urlImgs = urlImgs;
		this.origin = origin;
		this.trademark = trademark;
		this.shortInfomation = shortInfomation;
		this.longFunc = longFunc;
		this.using = using;
		this.date = date;
	}

	/**
	 * @param iDCatalog
	 * @param fullName
	 * @param description
	 * @param originPrice
	 * @param salePrice
	 * @param function
	 * @param numbers
	 * @param urlImg
	 * @param urlImgs
	 * @param origin
	 * @param trademark
	 * @param shortInfomation
	 * @param longFunc
	 * @param using
	 */
	public Product(String iDCatalog, String fullName, String description, int originPrice, int salePrice,
			String function, int numbers, String urlImg, String urlImgs, String origin, String trademark,
			String shortInfomation, String longFunc, String using, Date date) {
		super();
		this.iDCatalog = iDCatalog;
		this.fullName = fullName;
		this.description = description;
		this.originPrice = originPrice;
		this.salePrice = salePrice;
		this.function = function;
		this.numbers = numbers;
		this.urlImg = urlImg;
		this.urlImgs = urlImgs;
		this.origin = origin;
		this.trademark = trademark;
		this.shortInfomation = shortInfomation;
		this.longFunc = longFunc;
		this.using = using;
		this.date = date;
	}

	public String getiD() {
		return iD;
	}

	public void setiD(String iD) {
		this.iD = iD;
	}

	public String getiDCatalog() {
		return iDCatalog;
	}

	public void setiDCatalog(String iDCatalog) {
		this.iDCatalog = iDCatalog;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getOriginPrice() {
		return originPrice;
	}

	public void setOriginPrice(int originPrice) {
		this.originPrice = originPrice;
	}

	public int getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(int salePrice) {
		this.salePrice = salePrice;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public int getNumbers() {
		return numbers;
	}

	public void setNumbers(int numbers) {
		this.numbers = numbers;
	}

	public String getUrlImg() {
		return urlImg;
	}

	public void setUrlImg(String urlImg) {
		this.urlImg = urlImg;
	}

	public String getUrlImgs() {
		return urlImgs;
	}

	public void setUrlImgs(String urlImgs) {
		this.urlImgs = urlImgs;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getTrademark() {
		return trademark;
	}

	public void setTrademark(String trademark) {
		this.trademark = trademark;
	}

	public String getShortInfomation() {
		return shortInfomation;
	}

	public void setShortInfomation(String shortInfomation) {
		this.shortInfomation = shortInfomation;
	}

	public String getLongFunc() {
		return longFunc;
	}

	public void setLongFunc(String longFunc) {
		this.longFunc = longFunc;
	}

	public String getUsing() {
		return using;
	}

	public void setUsing(String using) {
		this.using = using;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Product [iD=" + iD + ", iDCatalog=" + iDCatalog + ", fullName=" + fullName + ", description="
				+ description + ", originPrice=" + originPrice + ", salePrice=" + salePrice + ", function=" + function
				+ ", numbers=" + numbers + ", urlImg=" + urlImg + ", urlImgs=" + urlImgs + ", origin=" + origin
				+ ", trademark=" + trademark + ", shortInfomation=" + shortInfomation + ", longFunc=" + longFunc
				+ ", using=" + using + ", date=" + date + "]";
	}
	
}
