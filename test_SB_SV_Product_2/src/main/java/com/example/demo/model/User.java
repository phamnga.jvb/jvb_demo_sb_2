package com.example.demo.model;

public class User {

	private String iD;
	private String userName;
	private String password;
	private String email;
	private String fullName;
	private String care;
	private String address;
	private String phone;

	public User(String userName, String password, String email, String fullName, String care, String address,
			String phone) {
		super();
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.fullName = fullName;
		this.care = care;
		this.address = address;
		this.phone = phone;
	}

	public String getiD() {
		return iD;
	}

	public void setiD(String iD) {
		this.iD = iD;
	}

	public User(String iD, String userName, String password, String email, String fullName, String care, String address,
			String phone) {
		super();
		this.iD = iD;
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.fullName = fullName;
		this.care = care;
		this.address = address;
		this.phone = phone;
	}

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCare() {
		return care;
	}

	public void setCare(String care) {
		this.care = care;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "User [iD=" + iD + ", userName=" + userName + ", password=" + password + ", email=" + email
				+ ", fullName=" + fullName + ", care=" + care + ", address=" + address + ", phone=" + phone + "]";
	}
	
}
