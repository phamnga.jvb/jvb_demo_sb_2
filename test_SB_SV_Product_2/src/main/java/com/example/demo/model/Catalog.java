package com.example.demo.model;

public class Catalog {
	
	private String iD;
	private String parentID;
	private String name;
	private String des;
	private String urlImg;
	
	public Catalog() {
		super();
	}

	public Catalog(String iD, String parentID, String name, String des, String urlImg) {
		super();
		this.iD = iD;
		this.parentID = parentID;
		this.name = name;
		this.des = des;
		this.urlImg = urlImg;
	}	
	
	/**
	 * @param parentID
	 * @param name
	 */
	public Catalog(String parentID, String name, String des, String urlImg) {
		super();
		this.parentID = parentID;
		this.name = name;
		this.des = des;
		this.urlImg = urlImg;
	}

	public String getiD() {
		return iD;
	}

	public void setiD(String iD) {
		this.iD = iD;
	}

	public String getParentID() {
		return parentID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public String getName() {
		return name;
	}
	
	public String getDes() {
		return des;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setDes(String des) {
		this.des = des;
	}	
	
	public String getUrlImg() {
		return urlImg;
	}

	public void setUrlImg(String urlImg) {
		this.urlImg = urlImg;
	}

	@Override
	public String toString() {
		return "Catalog [iD=" + iD + ", parentID=" + parentID + ", name=" + name + ", des=" + des + ", urlImg=" + urlImg + "]";
	}	
	
}
