package com.example.demo.model;

public class Ward {

	private String Id;
	private String WardName;
	private String WardType;
	private String DistrictId;
	
	public Ward(String id, String wardName, String wardType, String districtId) {
		super();
		Id = id;
		WardName = wardName;
		WardType = wardType;
		DistrictId = districtId;
	}

	public Ward() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getWardName() {
		return WardName;
	}

	public void setWardName(String wardName) {
		WardName = wardName;
	}

	public String getWardType() {
		return WardType;
	}

	public void setWardType(String wardType) {
		WardType = wardType;
	}

	public String getDistrictId() {
		return DistrictId;
	}

	public void setDistrictId(String districtId) {
		DistrictId = districtId;
	}	
	
}
