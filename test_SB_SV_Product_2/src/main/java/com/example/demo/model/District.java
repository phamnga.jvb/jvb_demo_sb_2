package com.example.demo.model;

public class District {

	private String Id;
	private String DistrictName;
	private String DistricType;
	private String RegionId;

	public District(String id, String districtName, String districType, String regionId) {
		super();
		Id = id;
		DistrictName = districtName;
		DistricType = districType;
		RegionId = regionId;
	}

	public District() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getDistrictName() {
		return DistrictName;
	}

	public void setDistrictName(String districtName) {
		DistrictName = districtName;
	}

	public String getDistricType() {
		return DistricType;
	}

	public void setDistricType(String districType) {
		DistricType = districType;
	}

	public String getRegionId() {
		return RegionId;
	}

	public void setRegionId(String regionId) {
		RegionId = regionId;
	}

}
