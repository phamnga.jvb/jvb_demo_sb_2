package com.example.demo.model;

public class Order {

	private String iD;
	private String iDTransaction;
	private String iDProduct;
	private int numbers;
	private int amount;
	private String comment;
	private String status;
	
	public Order(String iD, String iDTransaction, String iDProduct, int numbers, int amount, String comment,
			String status) {
		super();
		this.iD = iD;
		this.iDTransaction = iDTransaction;
		this.iDProduct = iDProduct;
		this.numbers = numbers;
		this.amount = amount;
		this.comment = comment;
		this.status = status;
	}

	public Order(String iDTransaction, String iDProduct, int numbers, int amount, String comment, String status) {
		super();
		this.iDTransaction = iDTransaction;
		this.iDProduct = iDProduct;
		this.numbers = numbers;
		this.amount = amount;
		this.comment = comment;
		this.status = status;
	}

	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getiD() {
		return iD;
	}

	public void setiD(String iD) {
		this.iD = iD;
	}

	public String getiDTransaction() {
		return iDTransaction;
	}

	public void setiDTransaction(String iDTransaction) {
		this.iDTransaction = iDTransaction;
	}

	public String getiDProduct() {
		return iDProduct;
	}

	public void setiDProduct(String iDProduct) {
		this.iDProduct = iDProduct;
	}

	public int getNumbers() {
		return numbers;
	}

	public void setNumbers(int numbers) {
		this.numbers = numbers;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Order [iD=" + iD + ", iDTransaction=" + iDTransaction + ", iDProduct=" + iDProduct + ", numbers="
				+ numbers + ", amount=" + amount + ", comment=" + comment + ", status=" + status + "]";
	}
		
}
