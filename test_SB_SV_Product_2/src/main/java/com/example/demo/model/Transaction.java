package com.example.demo.model;

public class Transaction {

	private String iD;
	private String status;
	private String iDUser;
	private String nameUser;
	private String phoneUser;
	private String emailUser;
	private String address;
	private String created;
	private String amount; 
	private String content; 
	private String code; 
	private String payment; 
	private String payInfo;
	
	public Transaction(String status, String iDUser, String nameUser, String phoneUser, String emailUser,
			String address, String created, String amount, String content, String code, String payment, String payInfo) {
		super();
		this.status = status;
		this.iDUser = iDUser;
		this.nameUser = nameUser;
		this.phoneUser = phoneUser;
		this.emailUser = emailUser;
		this.address = address;
		this.created = created;
		this.amount = amount;
		this.content = content;
		this.code = code;
		this.payment = payment;
		this.payInfo = payInfo;
	}

	/**
	 * 
	 */
	public Transaction() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param iD
	 * @param status
	 * @param iDUser
	 * @param nameUser
	 * @param phoneUser
	 * @param emailUser
	 * @param created
	 * @param amount
	 * @param content
	 * @param code
	 * @param payment
	 * @param payInfo
	 */
	public Transaction(String iD, String status, String iDUser, String nameUser, String phoneUser, String emailUser,
			String address, String created, String amount, String content, String code, String payment, String payInfo) {
		super();
		this.iD = iD;
		this.status = status;
		this.iDUser = iDUser;
		this.nameUser = nameUser;
		this.phoneUser = phoneUser;
		this.emailUser = emailUser;
		this.address = address;
		this.created = created;
		this.amount = amount;
		this.content = content;
		this.code = code;
		this.payment = payment;
		this.payInfo = payInfo;
	}

	public String getiD() {
		return iD;
	}

	public void setiD(String iD) {
		this.iD = iD;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIDUser() {
		return iDUser;
	}

	public void setIDUser(String iDUser) {
		this.iDUser = iDUser;
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	public String getPhoneUser() {
		return phoneUser;
	}

	public void setPhoneUser(String phoneUser) {
		this.phoneUser = phoneUser;
	}

	public String getEmailUser() {
		return emailUser;
	}

	public void setEmailUser(String emailUser) {
		this.emailUser = emailUser;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getPayInfo() {
		return payInfo;
	}

	public void setPayInfo(String payInfo) {
		this.payInfo = payInfo;
	}	
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Transaction [iD=" + iD + ", status=" + status + ", iDUser=" + iDUser + ", nameUser=" + nameUser
				+ ", phoneUser=" + phoneUser + ", emailUser=" + emailUser + ", address=" + address + ", created=" + created + ", amount="
				+ amount + ", content=" + content + ", code=" + code + ", payment=" + payment + ", payInfo=" + payInfo
				+ "]";
	} 	
}
